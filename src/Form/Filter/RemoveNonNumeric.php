<?php
/**
 * Form Filter StringTrim
 */

namespace Sharecoto\Form\Filter;

class RemoveNonNumeric extends FilterAbstract
{
    public function filter($value)
    {
        $regexp = '/[^\d]*/';
        return preg_replace($regexp, '', $value);
        return $this->removeNonNumeric($value);
    }
}
