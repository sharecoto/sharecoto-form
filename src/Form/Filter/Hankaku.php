<?php
/**
 * Form Filter Hankaku
 *
 * 英数字を全部半角にする
 */

namespace Sharecoto\Form\Filter;

class Hankaku extends FilterAbstract
{
    public function filter($value)
    {
        return $this->convertToHankaku($value);
    }

    public function convertToHankaku($value, $option = 'as', $encoding = "UTF-8")
    {
        return mb_convert_kana($value, $option, $encoding);
    }
}


