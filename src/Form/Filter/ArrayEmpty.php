<?php
/**
 * Form Filter ArrayEmpty
 *
 * 空の配列要素を削除する
 */

namespace Sharecoto\Form\Filter;

class ArrayEmpty extends FilterAbstract
{
    public function filter($value)
    {
        if (!is_array($value)) {
            throw new \InvalidArgumentException();
        }

        return array_filter($value, function($v) {
            if ($v === '') {
                return false;
            }
            return true;
        });

    }

}
