<?php
/**
 * フォームのフィルター抽象クラス
 */

namespace Sharecoto\Form\Filter;

abstract class FilterAbstract
{
    abstract public function filter($value);
}
