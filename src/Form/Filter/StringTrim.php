<?php
/**
 * Form Filter StringTrim
 */

namespace Sharecoto\Form\Filter;

class StringTrim extends FilterAbstract
{
    public function filter($value)
    {
        return $this->stringTrim($value);
    }

    protected function stringTrim($value, $mask=" \t\n\r\0\x0B")
    {
        return trim((string)$value, $mask);
    }
}
