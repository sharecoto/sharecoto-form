<?php
/**
 * Form Filter StripTags
 */

namespace Sharecoto\Form\Filter;

class StripTags extends FilterAbstract
{
    public function filter($value)
    {
        return $this->stripTags($value);
    }

    protected function stripTags($value, $allowable_tags = null)
    {
        return strip_tags((string)$value, $allowable_tags);
    }
}
