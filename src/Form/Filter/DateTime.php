<?php
/**
 * Form Filter DateTime
 */

namespace Sharecoto\Form\Filter;

class DateTime extends FilterAbstract
{
    public function filter($value)
    {
        return $this->dateToIso8601($value);
    }

    public function dateToIso8601($value)
    {
        $date = new \DateTime($value);
        return $date->format('c');
    }
}
