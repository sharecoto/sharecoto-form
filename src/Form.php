<?php

namespace Sharecoto;

use Sharecoto\Request\RequestAbstract;
use JsonSchema\Validator;
use JsonSchema\Uri\UriRetriever;
use JsonSchema\Constraints\Factory;
use JsonSchema\Constraints\Constraint;

class Form
{
    protected $formData;
    protected $schemaPath;
    protected static $defaultSchemaPath;
    protected $schemaFile;
    protected $schema;
    protected static $checkMode = Constraint::CHECK_MODE_COERCE_TYPES;

    public function __construct(RequestAbstract $formData = null, $schemaPath = null, UriRetriever $retriever = null, $autoFilter = true)
    {
        $this->schemaPath = $schemaPath;

        $this->formData = $formData;

        // デフォルトのretriever
        if (!$retriever) {
            $retriever = new UriRetriever();
        }
        $this->retriever = $retriever;

        // schemaをセット
        if ($this->schemaFile) {
            $this->setSchema($this->schemaFile);
        }

        // フィルタ
        if (isset($this->schema->properties) && $autoFilter) {
            $this->filterAll();
            $this->numberFilter();
        }
    }

    /**
     * defaultSchemaPathをセット.
     *
     * @param string $path
     */
    public static function setDefaultSchemaPath($path)
    {
        if (!is_dir($path)) {
            throw new \Exception('$path is not valid path');
        }
        self::$defaultSchemaPath = $path;
    }

    /**
     * defaultSchemaPath
     *
     * @return string
     */
    public static function getDefaultSchemaPath()
    {
        return self::$defaultSchemaPath;
    }

    public static function setCheckMode(integer $mode)
    {
        self::$checkMode = $mode;
    }

    public function validatorInstance()
    {
        $factory = new Factory(null, null, self::$checkMode);
        return new Validator($factory);
    }

    public function setSchema($file = null)
    {
        if (!$file) {
            $file = $this->schemaFile;
        }

        $path = $this->schemaPath ? $this->schemaPath : self::$defaultSchemaPath;

        if (!$path) {
            throw new \Exception('$path in not set.');
        }

        $url = sprintf(
            'file://%s',
            realpath($path.'/'.$file)
        );
        $this->schema = $this->retriever->retrieve($url);

        return $this;
    }

    public function getSchema()
    {
        return $this->schema;
    }

    public function setFormData(Request\RequestAbstract $formData)
    {
        $this->formData = $formData;

        return $this;
    }

    /**
     * @return \Sharecoto\Request\RequestAbstract
     */
    public function getFormData()
    {
        return $this->formData;
    }

    /**
     * @return \JsonSchema\Validator
     */
    public function validate()
    {
        $validator = $this->validatorInstance();

        $data = json_decode($this->formData->toJson());
        $validator->check($data, $this->schema);

        return $validator;
    }

    /**
     * JsonSchema\Validatorで、checktypeを変更しても
     * 数字の文字列を'string'と判定して、
     * バリデーションを通してくれないので、
     * integerもしくはdoubleに変換するフィルタ書く。
     */
    public function numberFilter()
    {
        foreach ($this->schema->properties as $key => $val) {
            if ($val->type === 'integer' && is_numeric($this->formData->$key)) {
                $this->formData[$key] = (int) $this->formData[$key];
            }

            if ($val->type === 'number' && is_numeric($this->formData->$key)) {
                $this->formData[$key] = (float) $this->formData[$key];
            }
        }

        return $this;
    }

    /**
     * JsonSchema\Validatorが、requiredを指定しても空文字列を
     * 通してしまうので、空文字列の要素を削除してしまうようにした
     * "minLength"を指定してもいいけどなんか違うので.
     */
    public function notEmptyFilter()
    {
        if (!$this->formData) {
            return $this;
        }

        $class = get_class($this->formData);

        $filter = function ($arr) use (&$filter) {
            $result = [];
            foreach ($arr as $key => $val) {
                if (is_array($val)) {
                    $result[$key] = $filter($val);
                    continue;
                }
                if ($val === '') {
                    continue;
                }
                $result[$key] = $val;
            }

            return $result;
        };

        $formData = $filter($this->formData->getArrayCopy());
        $this->formData = new $class($formData);

        return $this;
    }

    /**
     * フィルタリング全部入り.
     *
     * JSonSchemaでform_filterが設定されてたら、
     * 該当するFirm\Filterクラスのfilter()メソッドを呼び、
     * $formDataを更新する
     */
    public function filterAll()
    {
        foreach ($this->schema->properties as $key => $value) {
            if (empty($value->form_filter)) {
                continue;
            }
            $this->filter($key, $value->form_filter);
        }

        if (!isset($this->allowEmpty) || !$this->allowEmpty) {
            $this->notEmptyFilter();
        }

        return $this;
    }

    /**
     * 個別の要素ごとのフィルタ
     *
     * @param string $key     フィルタリング対象のフォームデータのキー
     * @param array  $filters jsonSchemaで定義されてるフィlルタ名
     *
     * @return $this
     */
    public function filter($key, $filters)
    {
        foreach ($filters as $f) {
            if (empty($this->formData[$key])) {
                continue;
            }
            $class = '\Sharecoto\Form\Filter\\'.ucfirst($f);
            $filterInstance = new $class();
            $this->formData[$key] = $filterInstance->filter($this->formData[$key]);
        }

        return $this;
    }

    /**
     * エラーメッセージ取得.
     */
    public function getErrors()
    {
        if (!$this->validate()->getErrors()) {
            return null;
        }

        // PHP5.5が使えるならこうしたいところ
        //return array_column($this->validate()->getErrors(), 'message', 'property');

        // PHP5.4対応
        $errors = [];
        foreach ($this->validate()->getErrors() as $v) {
            $errors[$v['property']] = $v['message'];
        }

        return $errors;
    }
}
