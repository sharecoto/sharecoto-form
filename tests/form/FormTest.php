<?php
use Sharecoto\Form;
use Sharecoto\Request\Post;
use PHPUnit\Framework\TestCase;

class FormTest extends TestCase
{
    public function setUp()
    {
    }

    public function testDefaultSchemaPath()
    {
        $path = realpath(__DIR__ . '/../sample');
        Form::setDefaultSchemaPath($path);

        $defaultPath = Form::getDefaultSchemaPath();

        $this->assertEquals($path, $defaultPath);
    }

    public function testValidator()
    {
        $path = realpath(__DIR__ . '/../sample');
        Form::setDefaultSchemaPath($path);

        $data = json_decode(file_get_contents($path . '/data.json'), true);
        $post = new Post($data);

        $form = new class($post) extends Form
        {
            protected $schemaFile = 'schema.json';
        };

        $validator = $form->validate();
        $this->assertInstanceOf('JsonSchema\Validator', $validator);
    }
}
